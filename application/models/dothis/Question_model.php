<?php

/**
 * Created by PhpStorm.
 * User: SuenYuen
 * Date: 9/4/2016
 * Time: 上午12:51
 */
class Question_model extends CI_Model
{
    private static $startQid = 2;
    private static $endQid = 134;

    public function __construct()
    {

    }

    public function getQuestions($num){
        $arr = $this->randQid($num);
        $str = '';
        foreach($arr as $value){
            $str = $str . ', ' . $value;
        }
        $str = substr($str, 2);
        $sql = "SELECT * FROM QuestionBase where questionid in($str)";
        $url = 'http://jokey-question.3eeweb.com/connect.php';
        $data = array('query_string' => $sql);

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
//        if ($result === FALSE) { }

        return json_decode($result);
    }

//    public function getQuestion($qid){
//
//    }

    private function randQid($num){
        $arr = array();
        $a = 0;
        if($num > self::$endQid - self::$startQid){
            $num = self::$endQid - self::$startQid;
        }
        while($a <= $num){
            $rand = rand(self::$startQid, self::$endQid);
            if(isset($arr[$rand])){
                continue;
            }
            $arr[$rand] = $rand;
            $a++;
        }
        ksort($arr);
        return $arr;
    }

//    public function get_news($slug = FALSE)
//    {
//        if ($slug === FALSE)
//        {
//            $query = $this->db->get('news');
//            return $query->result_array();
//        }
//
//        $query = $this->db->get_where('news', array('slug' => $slug));
//        return $query->row_array();
//    }
//
//    public function set_news()
//    {
//        $this->load->helper('url');
//
//        $slug = url_title($this->input->post('title'), 'dash', TRUE);
//
//        $data = array(
//            'title' => $this->input->post('title'),
//            'slug' => $slug,
//            'text' => $this->input->post('text')
//        );
//
//        return $this->db->insert('news', $data);
//    }
}