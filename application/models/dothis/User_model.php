<?php

/**
 * Created by PhpStorm.
 * User: SuenYuen
 * Date: 9/4/2016
 * Time: 上午12:51
 */
class User_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->helper('security');
    }

    public function login($username, $password){
        $md5_password = do_hash($password, 'md5');
        $query = $this->db->get_where('user', array('user_name' => $username, 'password' => $md5_password));
        return $query->row_array();
    }
    
//    public function get_news($slug = FALSE)
//    {
//        if ($slug === FALSE)
//        {
//            $query = $this->db->get('news');
//            return $query->result_array();
//        }
//
//        $query = $this->db->get_where('news', array('slug' => $slug));
//        return $query->row_array();
//    }
//
//    public function set_news()
//    {
//        $this->load->helper('url');
//
//        $slug = url_title($this->input->post('title'), 'dash', TRUE);
//
//        $data = array(
//            'title' => $this->input->post('title'),
//            'slug' => $slug,
//            'text' => $this->input->post('text')
//        );
//
//        return $this->db->insert('news', $data);
//    }
}