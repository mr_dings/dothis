<?php
/**
 * Created by PhpStorm.
 * User: SuenYuen
 * Date: 9/4/2016
 * Time: 下午10:03
 */

if (!function_exists('getImg_url')) {
    function getImg_url($img_name, $view_dir = '')
    {
        if (empty($view_dir)) {
            $url = "http://{$_SERVER['HTTP_HOST']}/application/img/" . $img_name ;
        } else {
            $url = "http://{$_SERVER['HTTP_HOST']}/application/views/" . $view_dir . '/img/' . $img_name;
        }
        return $url;
    }
}
