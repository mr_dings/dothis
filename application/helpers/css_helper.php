<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('test')) {
    function test()
    {
        echo getcwd();
    }
}

if (!function_exists('getcss_url')) {
    function getcss_url($css_name, $view_dir = '')
    {
        if (empty($view_dir)){
            $url = "http://{$_SERVER['HTTP_HOST']}/application/css/" . $css_name . '.css';
        }else
            $url = "http://{$_SERVER['HTTP_HOST']}/application/views/" . $view_dir . '/css/' . $css_name . '.css';

        return $url;
    }
}


if (!function_exists('create_css_tag')) {
    function create_css_tag($css_name, $view_dir = '')
    {
        $url = getcss_url($css_name,$view_dir);
        $tag = "<link rel='stylesheet' type='text/css' href='$url'/>";
        echo $tag;
    }
}