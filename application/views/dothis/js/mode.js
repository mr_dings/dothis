/**
 * Created by SuenYuen on 10/4/2016.
 */

$(document).ready(function () {
    $("table.questions a").click(function () {
        var next_question = $("div.questions div.n_quest span:first-child").text();
        var current_question = parseInt(next_question) - 1;
        var current_ans = $(this).attr("name");
        console.log(current_ans + "|||" + current_question);
        var input_question = $('<input/>').attr('type', 'hidden').attr('name', "current_question").attr('value', current_question);
        var input_ans = $('<input/>').attr('type', 'hidden').attr('name', "current_ans").attr('value', current_ans);
        input_ans.appendTo('#F1');
        input_question.appendTo('#F1');
        console.log(current_question);
        if (current_question >= 44) {
            next_question = 44;
        }
        $("form#F1").attr('action', '/dothis/game/' + next_question).submit();
    });
    var next_question = $("div.questions div.n_quest span:first-child").text();
    var current_question = parseInt(next_question) - 1;
    var current_ans = $(this).attr("name");
    var colum_position = current_question % 5 + 1;
    var row_position = Math.floor(current_question / 5) + 1;
    console.log(row_position + " || " + colum_position);
    $("table.tbp_quest tbody tr:nth-child(" + row_position + ") td:nth-child(" + colum_position + ")").removeClass().addClass('current_question');

    var timezone = $("div.timer span");
    var remainTime = timezone.text();
    remainTime = parseInt(remainTime);
    if (remainTime <= 0) {
        remainTime = 0;
    }
    // console.log(remainTime);
    var HMS = secondsTimeSpanToHMS(remainTime);
    $(timezone).text(HMS);
    var countdownF = function countdownTimer() {
        var timezone = $("div.timer span");
        var remainTime = timezone.text();

        var dateTime = remainTime.split(":");
        var second = parseInt(dateTime[0]) * 60 + parseInt(dateTime[1]) - 1;

        if (second <= 0) {
            second = 0;
            timesUp("times up",function(){
                console.log('times up aciton...');
                self.location =  "/dothis/review";
            });
            clearInterval(TimerId);
        }
        var HMS = secondsTimeSpanToHMS(second);
        // console.log(HMS);
        $(timezone).text(HMS);
    };
    var TimerId = setInterval(countdownF, 1000);

});

function timesUp(damsg, callback) {
    alert(damsg);
    if (typeof callback == "function")
        callback();
}

function endGame() {
    self.location = "/dothis/review";
}

function secondsTimeSpanToHMS(s) {
    // var h = Math.floor(s/3600); //Get whole hours
    // s -= h*3600;
    var m = Math.floor(s / 60); //Get remaining minutes
    s -= m * 60;
    return (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
}