<!DOCTYPE html>
<html>

<?php create_css_tag('index', 'dothis') ?>
<?php create_css_tag('mode', 'dothis'); ?>

<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
</head>
<body>
<div id="content">
    <div id="head">
        <div class="logo">
            <a title="dothis" href="/">
                <img title="dothis" src="<?php echo getImg_url('logo1.png', 'dothis') ?>"/>
            </a>
        </div>
        <div class="menu">
            <?php
            if (isset($_COOKIE['user_name'])) {
                ?>
                    <a href="#">hi <?php echo $_COOKIE['user_name']; ?></a>
                <?php
            }
            ?>
            <a href="/">Home</a>
            <div class="delim">&nbsp;</div>

            <?php
            if (isset($_COOKIE['user_name'])) {
                ?>
                <a href="/dothis/slogout">Logout</a>
                <?php
            } else {
                ?>
                <a href="/dothis/login">Login</a>
                <?php
            }
            ?>
        </div>
    </div>