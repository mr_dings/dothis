
    <div id="content" class="index">
        <h1>DoThis</h1>
        <div class="texts_left">
            <div>It is standard mathematics test and aims to help students to prepare their DSE.</div>
            <div>The test is designed as same as DSE which contains 45 questions and 75 minutes limit.</div>
            <div>This website is only available for standard mode. More mode can be found on mobile version.</div>
        </div>

        <div class="texts_right">
            <div><a href="/dothis/game">Start</a></div>
<!--            <div><a href="/dothis/review">Review</a></div>-->
            <div><a href="#"></a></div>
        </div>
    </div>
