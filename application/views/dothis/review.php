<div id="content" class="inner">
    <div class="contents" cellpadding="6" cellspacing="5">
        <div class="angles angle_l_t">&nbsp;</div>
        <div class="angles angle_r_t">&nbsp;</div>
        <div class="angles angle_l_b">&nbsp;</div>
        <div class="angles angle_r_b">&nbsp;</div>
        </br>
<!--        <span style="color:white;font-size:25px">Report</span>-->
        </br>
        <table class="time" align="right" border="0">
            <tbody>
            <tr>
                <td>Finish time:</td>
                <td><?php echo $finish_time; ?>s</td>
            </tr>
            <tr>
                <td>Average used time:</td>
                <td><?php echo $finish_time / 45; ?>s</td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="/dothis/clean" style="color:#018CF7; font-size: 40px">start a new test</a>
                </td>
            </tr>
            </tbody>
        </table>

        <table class="report" border="0">
            <tbody>
            <tr>
                <th>Question No.</th>
                <th>Right/Wrong</th>
                <th>Rate of correct</th>
            </tr>
            <?php
                for($i=0;$i<45;$i++){
                    ?>
            <tr>
                <td><?php echo $i + 1; ?></td>
                <td><?php echo $review_list[$i]['correct'] ? '&#10004' : 'x'; ?></td>
                <td><?php echo $review_list[$i]['correct_rate'] * 100 . '%'; ?></td>
                </tr>
                    <?php
                }
            ?>
            <tr>
                <td colspan="2"><h1>Score: <?php echo $correct_count; ?> / 45</h1></td>
<!--                <td><h1>XX</h1></td>-->
            </tr>
            </tbody>
        </table>
    </div>
</div>
		