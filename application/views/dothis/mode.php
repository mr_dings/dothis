<script type="text/javascript" src="/application/js/jquery-2.2.3.js"></script>
<script type="text/javascript" src="/application/views/dothis/js/mode.js"></script>
<div id="content" class="inner">
    <div class="contents" cellpadding="0" cellspacing="0">
        <table class="tbp_quest">
            <tbody>
            <?php
            for ($i = 0; $i < 45; $i++) {
                if ($i % 5 == 0) {
                    echo '<tr>';
                }
                ?>
                <td class="<?php echo $question_list[$i]->flat ? 'complete_question' : 'no_complete_question'; ?>">
                    <a href="/dothis/game/<?php echo $i; ?>"><?php echo $i + 1;?></a>
                </td>
                <?php
                if ($i % 5 == 4) {
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <div class="timer">Time remaining:</br><!----><span><?php echo $remain_time; ?></span></br></br><input onclick="endGame();" type="button" value="submit"/> </div>
        <div class="questions">
            <div class="n_quest">
                Question <span><?php echo $current_question + 1; ?></span> / <span>45</span></br>
            </div>
            <form id="F1" method="post" action="/dothis/game/gameRecord">
                <table class="questions" border="1" bordercolor="#84bfff" width="100%" cellpadding="10"
                       cellspacing="10">
                    <tbody>
                    <tr>
                        <td valign="top" height="300px" colspan="2"><?php echo $question_list[$current_question]->question; ?></td>
                    </tr>
                    <tr><!----------Js for click column------------>
                        <td><a href="#" name="a">a. <?php echo $question_list[$current_question]->choice_a; ?> </a></td>
                        <td><a href="#" name="b">b. <?php echo $question_list[$current_question]->choice_b; ?> </a></td>
                    </tr>
                    <tr>
                        <td><a href="#" name="c">c. <?php echo $question_list[$current_question]->choice_c; ?> </a></td>
                        <td><a href="#" name="d">d. <?php echo $question_list[$current_question]->ans; ?> </a></td>
                    </tr>
                    </tbody>
                </table>
            </form>
            <div class="break"></div>
        </div>
    </div>
