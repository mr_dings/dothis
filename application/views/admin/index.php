<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php create_css_tag('index', 'admin') ?>

<html>
<head>
    <title>index</title>
</head>

<body>
    <div class="container">
        <div class="left_menu">
            <h2>menu</h2>

            <ul>
                <li><a href="/admin/search" target="content_body">search</a></li>
                <li><a href="/admin/addQuestion" target="content_body">add question</a></li>
                <li><a href="/admin/feedback" target="content_body">feedback</a></li>
            </ul>
        </div>

        <div class="content">
            <iframe name="content_body" scrolling="auto"></iframe>
        </div>
    </div>
</body>
</html>
