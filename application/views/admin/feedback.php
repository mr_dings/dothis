<style>
td{
	padding: 5px 15px 0px 50px;
}
</style>
<table>
	<tr>
		<td>user name</td>
		<td>feed back message</td>
		<td>date</td>
		<td>is read</td>
	</tr>
	<?php
		include('db.php');
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		// Check connection
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		$sql = "select * from `feedback` ORDER BY is_read ASC, date DESC;";
		if ($reselt = mysqli_query($conn, $sql)) {
			while($feedback = $reselt->fetch_assoc()){
				?>
				<tr>
					<td><?php echo $feedback['user_name'];?></td>
					<td><?php echo $feedback['msg'];?></td>
					<td><?php echo $feedback['date'];?></td>
					<td><?php echo $feedback['is_read'] == 0 ? "no" : "yes";?></td>
					<td><a href="/admin/feedback/<?php echo $feedback['id']; ?>"><input type="button" value="read"/></a></td>
				</tr>
				<?php
			}
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	?>
</table>