<?php

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dothis/user_model');
        $this->load->model('dothis/question_model');
        $this->load->helper('cookie');
        $this->load->helper('css');
        session_start();
    }

    private function checkPermission()
    {
        $permission = $_COOKIE['permission'];
        return $permission == 1;
    }

    public function view($view = 'index.php', $data = array())
    {
        if ($view == 'index') {
            $this->index();
        } else {
            $this->load->view('admin/' . $view, $data);
        }
    }

    public function index()
    {
        if (isset($_COOKIE['user_name'])) {
            $is_permission = $this->checkPermission();
            if (!$is_permission) {
                echo 'you do not have permission to access';
                echo "<br/>";
                echo "<br/>";
                echo "click <a href='/'>here</a> return to index";
                return;
            } else {
                $this->load->view("admin/index.php");
            }
        } else {
            $this->load->view("admin/login_form.php");
        }
    }

    public function alogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if (!empty($username) && !empty($password)) {
            $data['user-info'] = $this->user_model->login($username, $password);
            if (empty($data['user-info'])) {
                $msg = array();
                $msg['login_status'] = 'username or password wrong';
            } else {
                $this->setCookies($data['user-info']);
                header('Location: ' . '/admin');
                return;
            }
        } else {
            $msg = array();
            if (empty($username)) {
                $msg['username'] = 'username can not be empty';
            }
            if (empty($password)) {
                $msg['password'] = 'password can not be empty';
            }
        }
        $data = array();
        $data['error_msg'] = $msg;
        $this->view('login_form.php', $data);
    }

    public function search($data = "")
    {
        if ($data == '') {
            $this->view("search.php");
        } elseif ($data == 'result') {
            $this->view("searchResult.php");
        }
    }

    public function addQuestion($mode = "")
    {
        if ($mode == '') {
            $this->view("addQuestion.php");
        } elseif ($mode == 'insert') {
            $this->view("db_action.php");
        }
    }

    public function feedback($id = -1){
        if($id < 0){
            $this->view("feedback.php");
            return;
        }else{
            $mdata = array(
                'is_read' => 1,
            );
            $this->db->where('id', $id);
            $this->db->update('feedback', $mdata);
            $this->view("feedback.php");
        }
    }

    private static function setCookies($data)
    {
        if (empty($data)) {
            return;
        }
        if (is_array($data)) {
            foreach ($data as $name => $value) {
                set_cookie($name, $value, 3000);
//                $this->setCookies($name,$value,60);
            }
        }
    }
}