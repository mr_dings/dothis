<?php

class Dothis extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('css');
        $this->load->helper('img');
        $this->load->model('dothis/user_model');
        $this->load->model('dothis/question_model');
        $this->load->helper('cookie');
        $this->load->helper('url');
        session_start();
    }

    public function index()
    {
        $data['title'] = 'dothis';
        $this->load->view('dothis/templates/header', $data);
        $this->load->view('dothis/index');
        $this->load->view('dothis/templates/footer');
    }

    public function view($view = 'index', $data = array())
    {
        if ($view == 'index') {
            $this->index();
        } else {
            $data['title'] = $view;
            $this->load->view('dothis/templates/header', $data);
            $this->load->view('dothis/' . $view, $data);
            $this->load->view('dothis/templates/footer');
        }
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if (!empty($username) && !empty($password)) {
            $data['user-info'] = $this->user_model->login($username, $password);
            if (empty($data['user-info'])) {
                $msg = array();
                $msg['login_status'] = 'username or password wrong';
            } else {
                $this->setCookies($data['user-info']);
                header('Location: ' . '/');
                return;
            }
        } else {
            $msg = array();
            if (empty($username)) {
                $msg['username'] = 'username can not be empty';
            }
            if (empty($password)) {
                $msg['password'] = 'password can not be empty';
            }
        }
        $data = array();
        $data['error_msg'] = $msg;
        $this->view('login', $data);
    }

    public function game($question = -1)
    {
        $time_limit = 4500;
//        echo base_url();
        $target_question = $this->input->post('current_question');
        $target_ans = $this->input->post('current_ans');
//        echo $target_question . '||||' . $target_ans;
        if ($target_question >= 0 && !empty($target_ans)) {
            $_SESSION['questions'][$target_question]->flat = true;
            $_SESSION['questions'][$target_question]->current_ans = $target_ans;
        }
        if ($question >= 0 && $question <= 45) {
            $_SESSION['current_question'] = $question;
        }
        if (isset($_SESSION['flat_game']) && $_SESSION['flat_game'] === true) {
//            echo '<pre>';
//            print_r($_SESSION['questions']);
//            echo '<pre/>';
//            echo time() - $_SESSION['start_time'];
//            echo '123';
            $data['remain_time'] = $_SESSION['start_time'] + $time_limit - time();
//            echo 'time : ' . $data['remain_time'];
            if ($data['remain_time'] <= 0 || $_SESSION['flat_end'] == true) {
                header('Location: ' . '/dothis/review');
                return;
            }
            $data['current_question'] = $_SESSION['current_question'];
            $data['question_list'] = $_SESSION['questions'];
            $this->view('mode', $data);
//            session_destroy();
        } else {
            $question_arr = $this->question_model->getQuestions(45);
            $question_arr = $this->init_question_arr($question_arr);
            $_SESSION['questions'] = $question_arr;
            $_SESSION['flat_game'] = true;
            $_SESSION['start_time'] = time();
            $_SESSION['current_question'] = 0;
            $_SESSION['flat_end'] = false;
            $data['remain_time'] = $_SESSION['start_time'] + $time_limit - time();
            $data['current_question'] = $_SESSION['current_question'];
            $data['question_list'] = $_SESSION['questions'];
            $this->view('mode', $data);
        }
    }

    public function review()
    {
        $data = array();
        if (!isset($_SESSION['finish_time'])) {
            $_SESSION['finish_time'] = time() - $_SESSION['start_time'];
            $_SESSION['flat_end'] = true;
        }
        $data['finish_time'] = $_SESSION['finish_time'];

        $review_list = array();
        $correct_count = 0;
        for ($i = 0; $i < 45; $i++) {
            if ($_SESSION['questions'][$i]->current_ans == 'd') {
                $review_list[$i]['correct'] = true;
                $correct_count++;
            } else {
                $review_list[$i]['correct'] = false;
            }
            if ($_SESSION['questions'][$i]->answered_count <= 0) {
                $review_list[$i]['correct_rate'] = 0;
            } else {
                $review_list[$i]['correct_rate'] = $_SESSION['questions'][$i]->correct_count / $_SESSION['questions'][$i]->answered_count;
            }
        }
        $data['review_list'] = $review_list;
        $data['correct_count'] = $correct_count;
        $this->view('review', $data);
//        echo '<pre>';
//        print_r($review_list);
//        echo '<pre/>';
    }

    public function logout()
    {

        delete_cookie('user_name');
//        foreach ( $_COOKIE as $key => $name )
//        {
//            delete_cookie($key);
//        }
        header('Location: ' . '/');
    }

    private function init_question_arr($arr)
    {
        foreach ($arr as $key => $value) {
//            echo "$key => " . $value->ans . '<br/>';
            $arr[$key]->flat = false;
            $arr[$key]->current_ans = null;
        }

        return $arr;
    }

    public function clean()
    {
        session_destroy();
        header('Location: ' . '/dothis/game');
    }

    private static function setCookies($data)
    {
        if (empty($data)) {
            return;
        }
        if (is_array($data)) {
            foreach ($data as $name => $value) {
                set_cookie($name, $value, 3000);
//                $this->setCookies($name,$value,60);
            }
        }
    }

}