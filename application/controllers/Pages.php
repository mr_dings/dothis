<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: SuenYuen
 * Date: 8/4/2016
 * Time: 下午7:18
 */
class Pages extends CI_Controller
{

    public function view($page = 'home')
    {
        if ( ! file_exists(APPPATH . 'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    
}